# Priority Queue Service

Queue Service implemented with DynamoDB to store and deliver messages to clients.


## Description

AWS has SQS to provide messaging send/deliver. But, sending to multiple clients implies to subscribe to an SNS topic, adding another component to configure, integrate, authorize, etc.

Also, SQS has some strange and unstable behaviors, not suitable for precise message sending.

DynamoDB, on the other hand, can implement more stable and optimistic behavior, and DAX can provide even more performance to the system.

In the future, this system can use other components as backends (such MongoDB, Redis, Google Cloud Store, Azure CosmosDB, etc), depending where or what cloud provider the system is running.


## References

* [Priority Queueing using DynamoDB](https://aws.amazon.com/pt/blogs/database/implementing-priority-queueing-with-amazon-dynamodb/)
* [DynamoDB partition key design](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/bp-partition-key-design.html)
* [DynamoDB parallel scans](https://aws.amazon.com/pt/blogs/aws/amazon-dynamodb-parallel-scans-and-other-good-news/)
* [Distributed Queue with DynamoDB vs SQS](https://stackoverflow.com/questions/12228056/writing-a-distributed-queue-in-amazons-dynamodb)
* [DAX features overview](https://aws.amazon.com/pt/blogs/database/amazon-dynamodb-accelerator-dax-a-read-throughwrite-through-cache-for-dynamodb/)
