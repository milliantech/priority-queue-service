/**
 * https://www.jamestharpe.com/serverless-typescript-getting-started/
 */

import {
  APIGatewayProxyHandler,
  APIGatewayEvent,
  Context /*, Callback*/
} from "aws-lambda";
import "source-map-support/register";

export const hello: APIGatewayProxyHandler = async (
  _event: APIGatewayEvent,
  _context: Context
) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!"
      },
      null, 2)
  };
};
